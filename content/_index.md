---
title: ""
date: 2022-11-12T15:18:26+01:00
draft: false
---

Télécharger la version extended du générateur [Hugo](https://gohugo.io/installation/).

Vérifier que le binaire hugo.exe se trouve dans un dossier accessible à `PATH`

```
hugo version

hugo v0.105.0-0e3b42b4a9bdeb4d866210819fc6ddcf51582ffa+extended windows/amd64 BuildDate=2022-10-28T12:29:05Z VendorInfo=gohugoio
```

Créer un nouveau site

```
hugo new site projet
```

Placer vous dans le dossier

```
cd projet
```

### Git

```shell-session
$ git init
```

Ajouter le fichier `.gitignore` à la racine du dossier

```
# Generated files by hugo
/public/
/resources/_gen/
/assets/jsconfig.json
hugo_stats.json

# Temporary lock file while building
/.hugo_build.lock
```

## Créer les pages

hugo new _index.md
hugo new variables\_index.md

Les pages créées sont dans le dossier `content`

Dans le dossier layouts/_default créer une page list.html
Le layout list.html est utilisé par le contenu _index


```html
<!doctype html>
<html class="no-js" lang="{{ .Site.Language.Lang }}">

<head>
  <meta charset="utf-8">
  <title>{{ .Title }} | {{ .Site.Title }}</title>
</head>

<body>
  <h1>{{ .Title }}</h1>
  {{ .Content }}
</body>
</html>
```


```shell-session
hugo -D
```

Les pages générées sont dans le dossier public

Ajouter dans config.toml

```toml
[markup.highlight]
  style = 'vs'
  tabWidth = 2
```

### Build avec Visual Studio Code

```json
{
	"version": "2.0.0",
	"tasks": [
		{
			"label": "echo",
			"type": "shell",
			"command": "hugo -D",
			"group": {
				"kind": "build",
				"isDefault": true
			}
		}
	]
}
```

Appuyez maintenant sur ctrl+maj+B pour lancer la compilation du site

### Publier qur Gitlab Pages

```yaml
image: registry.gitlab.com/pages/hugo/hugo_extended:latest

variables:
  GIT_SUBMODULE_STRATEGY: recursive

pages:
  script:
  - hugo
  artifacts:
    paths:
    - public
  rules:
  - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
```
